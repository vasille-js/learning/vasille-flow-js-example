// @flow
import { Component } from "vasille";

export class Page extends Component {
    $compose () {
        this.$tag("div", div => {
            div.$text('Welcome to Vasille demo');
            div.$debug(this.$ref("Look to me from dev tools"));
        });
    }
}
